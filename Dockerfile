FROM python:3.5-slim

WORKDIR /srv/my-simple-calculator

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

CMD [ "gunicorn", "--bind", "0.0.0.0:5000", "--access-logfile", "-", "wsgi:app" ]
